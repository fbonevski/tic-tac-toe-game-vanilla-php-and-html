#Tic Tac Toe Game

##Game Description

This is a simple tic tac toe game where you post X's or O's alternately and the winner is the first one to match three X's or O's vertically, horizontally or diagonally.
This game uses vanilla PHP and simple HTML.
