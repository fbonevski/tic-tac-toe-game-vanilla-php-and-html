<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>

    <body>
        <form method="POST" action="tic-tac-toe-game.php">
            <?php
            $error = false;
            $x_wins = false;
            $o_wins = false;
            $count = 0;
            for ($i = 1; $i <= 9; $i++)
            {
                if ($i == 4 or $i == 7) print "<br>";
                print "<input name = $i type = text size =8";
                if (isset($_POST['submit']) and !empty($_POST[$i]))
                {
                    if($_POST[$i] == "x" or $_POST[$i] == "o")
                    {
                        $count++;
                        print " value = ".$_POST[$i]." readonly>";
                        for($a = 1, $b = 2, $c = 3; $a <= 7, $b <= 8, $c <= 9; $a += 3, $b += 3, $c += 3)
                        {
                            if($_POST["$a"] == $_POST["$b"] and $_POST["$b"] == $_POST["$c"])
                            {
                                if($_POST["$a"] == 'x')
                                {
                                    $x_wins = true;
                                }
                                elseif($_POST["$a"] == 'o')
                                {
                                    $o_wins = true;
                                }
                            }
                        }

                        for($a = 1, $b = 4, $c = 7; $a <= 3, $b <= 6, $c <= 9; $a++, $b++, $c++)
                        {
                            if($_POST["$a"] == $_POST["$b"] and $_POST["$b"] == $_POST["$c"])
                            {
                                if($_POST["$a"] == 'x')
                                {
                                    $x_wins = true;
                                }
                                elseif($_POST["$a"] == 'o')
                                {
                                    $o_wins = true;
                                }
                            }
                        }
                        for($a = 1, $b = 5, $c = 9; $a <= 3, $b <= 5, $c >= 7; $a += 2,$b += 0, $c -= 2)
                        {
                            if($_POST["$a"] == $_POST["$b"] and $_POST["$b"] == $_POST["$c"])
                            {
                                if($_POST["$a"] == 'x')
                                {
                                    $x_wins = true;
                                }
                                elseif($_POST["$a"] == 'o')
                                {
                                    $o_wins = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        print">";
                        $error = true;
                    }
                }
                else
                {
                    print ">";
                }
            }
            ?>
            <p><input name="submit" type="submit"></p>
        </form>
        <?php
        if($x_wins)
        {
            print "Player X wins";
        }
        elseif($error)
        {
            print "It is game over if you do not enter x or o values";
        }
        elseif($o_wins)
        {
            print "Player O wins";
        }
        elseif($count == 9 and !$x_wins and !$o_wins)
        {
            print "Draw";
        }
        else
        {
            print "Please enter x or o values alternately";
        }
        ?>
    </body>
</html>